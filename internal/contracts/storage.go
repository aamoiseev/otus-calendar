package contracts

type Storage interface {
	Insert(key string, value interface{}) error
	Update(key string, value interface{}) error
	Delete(key string) (bool, error)
	Get(key string) (interface{}, error)
	List() ([]interface{}, error)
}
