package calendar

import (
	"fmt"
	"github.com/google/uuid"
	"gitlab.com/aamoiseev/otus-calendar/internal/contracts"
)

// Calendar store events
type Calendar struct {
	st contracts.Storage
}

// AddEvent add event to calendar
func (c *Calendar) AddEvent(e *Event) error {
	key, err := uuid.NewUUID()
	if err != nil {
		return err
	}

	e.Key = key.String()

	err = c.st.Insert(e.Key, *e)
	if err != nil {
		return err
	}

	return nil
}

// UpdateEvent updates event in calendar
func (c *Calendar) UpdateEvent(e Event) error {
	if e.Key == "" {
		return fmt.Errorf("event key must be provided")
	}

	return c.st.Update(e.Key, e)
}

// RemoveEvent removes event from calendar
func (c *Calendar) RemoveEvent(key string) (bool, error) {
	return c.st.Delete(key)
}

// GetEvent gets one event from calendar by key
func (c *Calendar) GetEvent(key string) (*Event, error) {
	record, err := c.st.Get(key)
	if err != nil {
		return nil, err
	}

	event, ok := record.(Event)
	if !ok {
		return nil, fmt.Errorf(`can't cast record "%v" to Event type`, record)
	}

	return &event, nil
}

// ListEvents fetch all events from calendar
func (c *Calendar) ListEvents() ([]Event, error) {
	records, err := c.st.List()
	if err != nil {
		return nil, err
	}

	events := make([]Event, len(records))
	for i, r := range records {
		event, ok := r.(Event)
		if !ok {
			return nil, fmt.Errorf(`can't cast record "%v" to Event type`, r)
		}

		events[i] = event
	}

	return events, nil
}

// NewCalendar creates new calendar
func NewCalendar(st contracts.Storage) *Calendar {
	return &Calendar{st: st}
}
