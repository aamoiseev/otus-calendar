package storage

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

var values = map[string]int{
	"one":   1,
	"two":   2,
	"three": 3,
}

func TestMemory_Insert(t *testing.T) {
	m := NewMemory()

	for k, v := range values {
		err := m.Insert(k, v)
		assert.Nil(t, err)

		val, err := m.Get(k)
		assert.Nil(t, err)

		assert.Equal(t, v, val)
	}
}

func TestMemory_Update(t *testing.T) {
	m := NewMemory()

	for k, v := range values {
		err := m.Insert(k, v)
		assert.Nil(t, err)
	}

	newValues := map[string]int{
		"one":   values["one"],
		"two":   5,
		"three": values["three"],
	}

	err := m.Update("two", newValues["two"])
	assert.Nil(t, err)

	for k, v := range newValues {
		val, err := m.Get(k)
		assert.Nil(t, err)

		assert.Equal(t, v, val)
	}
}

func TestMemory_Delete(t *testing.T) {
	m := NewMemory()

	for k, v := range values {
		err := m.Insert(k, v)
		assert.Nil(t, err)
	}

	ok, err := m.Delete("two")
	assert.Nil(t, err)
	assert.True(t, ok)

	v, err := m.Get("two")
	assert.Nil(t, v)
	assert.Error(t, err, `record with key "two" does not exist`)
}

func TestMemory_List(t *testing.T) {
	m := NewMemory()

	for k, v := range values {
		err := m.Insert(k, v)
		assert.Nil(t, err)
	}

	list, err := m.List()
	assert.Nil(t, err)
	assert.Len(t, list, len(values))

	orderedKeys := []string{"one", "two", "three"}

	for i, k := range orderedKeys {
		assert.Equal(t, values[k], list[i])
	}
}
